<?php

/**
 * CRUD opreations with wordpress posts
 *
 * Class Post
 */
class Post
{

    public $db;
    public $dbh;
    public $tableNames;

    /**
     * Create a new database connection
     */
    function __construct()
    {
        $this->db         = new DB();
        $this->dbh        = $this->db->connection;
        $this->tableNames = [
            'posts'              => 'wp_posts',
            'postmeta'           => 'wp_postmeta',
            'term_relationships' => 'wp_term_relationships',
            'term_taxonomy'      => 'wp_term_taxonomy',
            'terms'              => 'wp_terms'
        ];
    }

    /**
     * Добавление нового поста, обработка дефолтных и входящих значений
     *
     * @param array $postData
     */
    public function createPost($postData)
    {
        // дефолтные значения
        $defaults = [
            'post_date'             => date('o-m-j  h:i:s'),
            'author'                => 1,
            'post_status'           => 'publish',
            'comment_status'        => 'open',
            'ping_status'           => 'open',
            'post_name'             => str_replace(' ', '-', $postData['title']),
            'post_type'             => 'post',
            'comment_count'         => 0,
            'post_excerpt'          => '',
            'to_ping'               => '',
            'pinged'                => '',
            'post_content_filtered' => '',
            'mkey1'                 => null,
            'mkey2'                 => null
        ];

        // соединяем оба массива в один
        $inputs = array_merge($defaults, $postData);

        try {
            $sql = "INSERT INTO " . $this->tableNames['posts'] . " (post_author, post_date, post_date_gmt, post_content,
                                            post_title, post_status, comment_status, ping_status, post_name,
                                            post_modified, post_modified_gmt, guid, post_type, comment_count,
                                            post_excerpt, to_ping, pinged, post_content_filtered)
                    VALUES( :postAuthor, :postDate, :postDate, :postContent,  :postTitle, :postStatus,
                            :commentStatus, :pingStatus, :postName, :postDate, :postDate, '', :postType,
                            :commentCount, :postExcerpt, :toPing, :pinged, :postContentFiltered)";

            $data = $this->dbh->prepare($sql);

            $data->bindValue(':postAuthor', $inputs['author']);
            $data->bindValue(':postTitle', $inputs['title']);
            $data->bindValue(':postContent', $inputs['content']);
            $data->bindValue(':postDate', $inputs['post_date']);
            $data->bindValue(':postName', $inputs['post_name']);
            $data->bindValue(':postStatus', $inputs['post_status']);
            $data->bindValue(':commentStatus', $inputs['comment_status']);
            $data->bindValue(':pingStatus', $inputs['ping_status']);
            $data->bindValue(':postType', $inputs['post_type']);
            $data->bindValue(':commentCount', $inputs['comment_count']);
            $data->bindValue(':postExcerpt', $inputs['post_excerpt']);
            $data->bindValue(':toPing', $inputs['to_ping']);
            $data->bindValue(':pinged', $inputs['pinged']);
            $data->bindValue(':postContentFiltered', $inputs['post_content_filtered']);
            $data->execute();

            $postId = $this->dbh->lastInsertId();

            if ($postId) {
                $sqlMeta1 = "INSERT INTO " . $this->tableNames['postmeta'] . " (post_id, meta_key, meta_value)
                    VALUE (:postId, 'mkey_1', :mKey1)";

                $data = $this->dbh->prepare($sqlMeta1);
                $data->bindValue(':mKey1', $inputs['mkey1']);
                $data->bindValue(':postId', $postId);
                $data->execute();

                $sqlMeta2 = "INSERT INTO " . $this->tableNames['postmeta'] . " (post_id, meta_key, meta_value)
                    VALUE (:postId, 'mkey_2', :mKey2)";

                $data = $this->dbh->prepare($sqlMeta2);
                $data->bindValue(':mKey2', $inputs['mkey2']);
                $data->bindValue(':postId', $postId);
                $data->execute();
            }

            $this->dbh = null;

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function removePost()
    {

    }

    public function updatePost()
    {

    }

    /**
     * Вывод постов по заданным параметрам
     *
     * @param array $args Массив заданных параметров
     *
     * @return array $result
     */
    public function getPosts($args)
    {
        // дефолтные значения
        $defaults = [
            'sorting'        => 'DESC',
            'order_by'       => 'post_date',
            'search_string'  => '',
            'posts_per_page' => 5,
            'page_number'    => 1,
            'term_id'        => '',
            'term_slug'      => ''
        ];

        $terms  = [];
        $result = [];

        // удаляем элементы массива $args если они не походят по условиям
        if ($args['sorting'] != 'ASC') {
            unset($args['sorting']);
        }
        if ($args['order_by'] != 'post_title' && $args['order_by'] != 'ID') {
            unset($args['order_by']);
        }
        if ($args['posts_per_page'] == null || ! is_numeric($args['posts_per_page'])) {
            unset($args['posts_per_page']);
        }
        if ($args['page_number'] == null || ! is_numeric($args['page_number'])) {
            unset($args['page_number']);
        }

        // соединяем оба массива в один
        $inputs = array_merge($defaults, $args);

        $pageNumber = $inputs['posts_per_page'] * ($inputs['page_number'] - 1);

        if ($inputs['term_id']) {
            foreach ($inputs['term_id'] as $termId) {
                if (is_numeric($termId)) {
                    $terms[] = $termId;
                }
            }
            $terms = ' AND term.term_id IN (' . implode(',', $terms) . ') ';
        } elseif ($inputs['term_slug']) {
            $terms = ' AND term.slug IN ("' . $inputs['term_slug'] . '") ';
        } else {
            $terms = '';
        }

        try {
            $sql = 'SELECT p.post_title, p.post_date, term.name 
                    FROM ' . $this->tableNames['posts'] . ' p 
                    INNER JOIN ' . $this->tableNames['term_relationships'] . ' rel
                         ON rel.object_id = p.ID
                    INNER JOIN ' . $this->tableNames['term_taxonomy'] . ' tax
                         ON tax.term_taxonomy_id = rel.term_taxonomy_id
                    INNER JOIN ' . $this->tableNames['terms'] . ' term
                         ON term.term_id = tax.term_id  
                    WHERE  p.post_title LIKE :search
                    ' . $terms . ' 
                    ORDER BY ' . $inputs['order_by'] . ' ' . $inputs['sorting'] . ' 
                    LIMIT :postsPerPage ' . ' OFFSET :pageNumber ';

            $data = $this->dbh->prepare($sql);

            $data->bindValue(':search', "%{$inputs['search_string']}%");
            $data->bindValue(':postsPerPage', $inputs['posts_per_page'] + 1, PDO::PARAM_INT);
            $data->bindValue(':pageNumber', $pageNumber, PDO::PARAM_INT);
            $data->execute();

            // вывод данных в массив
            $result['list'] = $data->fetchAll();

            // проверка возможности вывода постов еще одной страницы
            if (count($result['list']) === $inputs['posts_per_page'] + 1) {
                $result['more'] = true;
                array_pop($result['list']);
            } else {
                $result['more'] = false;
            }

            $this->dbh = null;

            return $result;

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}
